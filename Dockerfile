#  - the build process
FROM node:9.11.2-alpine as build-deps


ENV NPM_CONFIG_LOGLEVEL warn

# Copy all local files into the image.
COPY . .

# Build for production.

RUN npm install -g -s --no-progress yarn && \
    yarn && \
    yarn run build && \
    yarn cache clean


RUN yarn global add serve



CMD serve -s build


# Tell Docker about the port we'll run on.
EXPOSE 5000