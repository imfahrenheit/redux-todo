import React from 'react';
import List from "@material-ui/core/List";
import { withStyles } from "@material-ui/core/styles";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import DoneIcon from "@material-ui/icons/Done";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";

const styles = theme => ({
  listItem: {
      background: "#80CBC4",
      width:"80%",
      margin:"0 auto"
  },
  doneHeading:{
      margin:"10px auto 0 auto"
    }, card: {
        marginTop: "5%",
      background: "none",
        boxShadow: "none"
    }
    
});
const EmptyTodoCard = ({ classes }) => {

    return <Card className={classes.card}>
        <CardContent className={classes.cardContent}>
            <h3> Your Done To-Do list is empty </h3>
        </CardContent>
    </Card>;

}

const DoneTodos = ({todos,classes}) => {
  const showCard= todos.every(el=>{
      return el.done===false
  })
    return (
        <div className='doneTodos' >
            <h2 className={classes.doneHeading} > Done List </h2>
            {todos.map(item => {

                    return item.done ? <List key={item.uid} >
                        <ListItem dense className={classes.listItem}>
                          <DoneIcon /> <ListItemText primary={item.todo} secondary={item.time} />
                        </ListItem>
                      </List> :null;
                })
            }
            { showCard?<EmptyTodoCard classes={classes}/> : null}
        </div>
    )
}

export default withStyles(styles)(DoneTodos)
