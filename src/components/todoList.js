import React from 'react';
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Checkbox from "@material-ui/core/Checkbox";
import { withStyles } from "@material-ui/core/styles";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';



const styles = theme => ({
  cardContent: {
    textAlign: "center"
  },
  listItem:{
  width:'80%', margin:" 0 auto"
  },
  card: {
    marginTop: "5%",
    background: "none",
    boxShadow: "none"
  },
  checked: {
    background: "rgb(255,152,0)",
    width:'80%', margin:" 0 auto"
  }
});

const EmptyTodoCard =({classes})=>{

    return <Card className={classes.card}>
        <CardContent className={classes.cardContent}>
          <h3> You have no To-Dos</h3>
          <p> Plase start adding To-Dos</p>
        </CardContent>
      </Card>;

    }

const TodoList = (props) => {

const {todos, classes, deleteTodo, toggleTodo}=props
  return (
    <div className='todolist' >
      {todos?todos.map((item)=>{
              return <List key={item.uid}>
                  <ListItem dense button className={item.done?`${classes.ListItem} ${classes.checked}`:classes.listItem}>
                    <Checkbox
                    onChange={()=>{toggleTodo(item.uid)}}
                
                    id={item.uid} />
                    <ListItemText primary={item.todo} secondary={item.time} />
                 
                    <DeleteForeverIcon
                    onClick={()=>{deleteTodo(item.uid)}}
                    id={item.uid} 
                    className={classes.icon} />
                      
                  </ListItem>
                </List>;

      }): null
      

      }
      {todos.length===0?<EmptyTodoCard classes={classes} />:null}
    </div>
  )
}

export default withStyles(styles)(TodoList);
