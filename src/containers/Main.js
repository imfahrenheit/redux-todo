import React, { Component } from "react";
import { connect } from "react-redux";
import UserInputs from "./userInputs";
import TodoList from "../components/todoList";
import DoneTodos from "../components/doneTodos"

import * as allActions from "../store/actions/actionCreators";


class Main extends Component {
  render() {
    const { todos, deleteTodo, toggleTodo} = this.props;
    
    return (
      <div className="container">
        <h1 className="heading"> Add Your To-Dos</h1>
        <div className="Main">
          <div className="user_inputs">
            <UserInputs addTodo={this.props.storeTodos} />
            <TodoList
            toggleTodo={toggleTodo}
            deleteTodo={deleteTodo} 
            todos={todos} />
          </div>
          <DoneTodos
            todos={todos}
            toggleTodo={toggleTodo}/>
        </div>
      </div>
    );
  }
}
const mapState = state => {
  return {
    todos: state.todoReducer.todos
    
  };
};
export default connect(
  mapState,
  allActions
)(Main);
