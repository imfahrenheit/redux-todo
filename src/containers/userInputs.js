import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import teal from "@material-ui/core/colors/teal";
import Button from "@material-ui/core/Button";
import AddIcon from "@material-ui/icons/Add";
import uuid from "uuid/v1";

import {
  withStyles,
  MuiThemeProvider,
  createMuiTheme
} from "@material-ui/core/styles";

const styles = theme => ({
  margin: {
    margin: theme.spacing.unit,
    width: "60%"
  },
  button: {
    marginLeft: "12%"
  }
});

const theme = createMuiTheme({
  palette: {
    primary:teal
  }
});

class UserInputs extends Component {
  state = {
    newTodo: {
      todo: "",
      time: "07:30"
    },
    error: false
  };

  onChangeHandler = e => {
    e.preventDefault();
    let { newTodo } = this.state,
      name = e.target.name,
      value = e.target.value;
    newTodo[name] = value;
    this.setState({ newTodo, error: false });
  };
  handleEnter = e => {
    if (e.key === "Enter") {
      this.addTodo();
    }
  };
  addTodo = () => {
    const { newTodo } = this.state;
    if (newTodo.todo === "" || newTodo.time === "") {
      this.setState({ error: !this.state.error });
      return;
    }
    newTodo.uid = uuid();
    newTodo.done = false;
    this.props.addTodo(newTodo);
    this.setState({
      newTodo: {
        todo: "",
        time: "07:30"
      },
      error: false
    });
  };

  render() {
    const { classes } = this.props;
    const { todo, time } = this.state.newTodo;
    const { error } = this.state;

    return (
      <div className="main-input">
        <MuiThemeProvider theme={theme}>
          <TextField
            error={error}
            onKeyPress={e => this.handleEnter(e)}
            className={classes.margin}
            label={!error ? " New To-do" : " Please enter a To-do"}
            id="mui-theme-provider-input"
            name="todo"
            value={todo}
            onChange={e => this.onChangeHandler(e)}
          />
        </MuiThemeProvider>

        <form noValidate>
          <MuiThemeProvider theme={theme}>
            <TextField
              error={error}
              name="time"
              value={time}
              id="time"
              label={!error ? " Time" : "Pick a Time"}
              type="time"
              onChange={e => this.onChangeHandler(e)}
              className={classes.margin}
              InputLabelProps={{
                shrink: true
              }}
              inputProps={{
                step: 500 // 5 min
              }}
            />
          </MuiThemeProvider>
        </form>

        <Button
          onClick={this.addTodo}
          variant="fab"
          mini
          color="primary"
          aria-label="Add"
        >
          <AddIcon />
        </Button>
      </div>
    );
  }
}
export default withStyles(styles)(UserInputs);
