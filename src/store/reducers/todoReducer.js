import * as actionTypes from "../actions/actionTypes";

const initialTodos = [
  {
    todo: "Wash clothes",
    time: "19:30",
    uid: "122dj33",
    done:false
  },
  {
    todo: "Throw  the trash",
    time: "09:30",
    uid: "1s22fdjdk",
    done: false
  },
  {
    todo: "Learn Go-lang",
    time: "20:30",
    uid: "12d9s2dk",
     done: false
  },
  {
    todo: "Buy food",
    time: "19:30",
    uid: "2939nndfjdk",
     done: false
  },
  {
    todo: "Check football match",
    time: "22:30",
    uid: "12fpmr0dk",
    done: false
  }
];

const initialState = {
  todos:initialTodos
  
};

export const todoReducer = (state = initialState, action) => {
  switch (action.type){


    case actionTypes.STORE_TODO:
      
      return { ...state, todos: [action.payload ,...state.todos]};

    case actionTypes.DELETE_TODO:
      
        const index1 = state.todos.findIndex(evt => evt.uid === action.payload);
      
        return {...state,todos:[...state.todos.slice(0, index1), ...state.todos.slice(index1 + 1)]};
      // there are shorter solution but I prefer this becuase it does not change the order the of dom elements after every update
    case actionTypes.TOGGLE_TODOS:
    
      return {
        ...state, todos: [...state.todos.map(item => {

          return item.uid !== action.payload?item:{...item,done: !item.done}
        })]
      }
      
    default:
      return state;
  }

};
