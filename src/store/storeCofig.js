import { createStore, applyMiddleware} from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import rootReducer from "./reducers/rootReducer";


const conFigureStore = preloadeState => {
  
  const middlewire = [];
  const middlewireEnhancer = applyMiddleware(...middlewire);
  //even Though there is no middleware in use at the moment, i just wanted to make the setup ready for scaling
  const storeEnhancer = [middlewireEnhancer];
  const composedEnhancer = composeWithDevTools(...storeEnhancer);

  const store = createStore(rootReducer, preloadeState, composedEnhancer);

  return store;
};
export default conFigureStore;
