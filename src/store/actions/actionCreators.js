import * as actionType from "./actionTypes";


// actions responsble for updating the redux store starts here 
export const storeTodos = todo => {
  return {
    type: actionType.STORE_TODO,
    payload:todo
  };
};

export const deleteTodo = (id)=> {
  return {
    type: actionType.DELETE_TODO,
    payload:id
    
  };
};
export const toggleTodo = (id) => {
  return {
    type: actionType.TOGGLE_TODOS,
    payload: id

  };
};
